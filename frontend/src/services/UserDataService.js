import http from "../http-common";

class UserDataService {
    getAll() {
        return http.get("/nutzers");
    }

    get(uid) {
        return http.get(`/nutzers/${uid}`);
    }

    create(data) {
        return http.post("/nutzers", data);
    }

    update(uid, data) {
        return http.put(`/nutzers/${uid}`, data);
    }

    delete(uid) {
        return http.delete(`nutzers/${uid}`);
    }

    deleteAll() {
        return http.delete(`/nutzers`);
    }

    findByName(name) {
        return http.get(`/nutzers/name/${name}`);
    }
}

export default new UserDataService();