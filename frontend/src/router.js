import { createRouter, createWebHistory } from 'vue-router'
// import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    alias: '/users',
    name: 'users',
    component: () => import("./components/UserList")
  },
  {
    path: '/users/:uid',
    name: 'user-details',
    component: () => import("./components/UserDetails.vue")
  },
  {
    path: '/add',
    name: 'add-user',
    component: () => import("./components/AddUser")
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
