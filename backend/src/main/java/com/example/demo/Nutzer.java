package com.example.demo;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="Nutzer")
public class Nutzer {
    @Id
    @SequenceGenerator(name="nutzer_uid_seq",
            sequenceName = "nutzer_uid_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "nutzer_uid_seq")
    @Column(name="uid", updatable = false)
    private Integer uid;
    private String email;
    private String name;

    protected Nutzer() {}

    public Nutzer(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public String toString() {
        return String.format(
                "Nutzer[uid=%d, name='%s, email='%s]",
                uid, name, email);
    }

}

