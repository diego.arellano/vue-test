package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin//(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api/")
public class NutzerController {
    @Autowired
    private NutzerRepository nutzerRepository;

    @GetMapping("/nutzers")
    public ResponseEntity<List<Nutzer>> getAllNutzers(){
        try {
            List<Nutzer> nutzers = new ArrayList<>(nutzerRepository.findAll());
            if (nutzers.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(nutzers, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/nutzers")
    public ResponseEntity<Nutzer> createNutzer(@RequestBody Nutzer nutzer){
        try {
            Nutzer n = nutzerRepository.save(
                new Nutzer(nutzer.getEmail(), nutzer.getName())
            );
            return new ResponseEntity<>(n, HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/nutzers/{uid}")
    public ResponseEntity<Nutzer> getNutzerByUid(@PathVariable("uid") Integer uid){
        Optional<Nutzer> nutzerData = nutzerRepository.findById(uid);
        return nutzerData.map(nutzer -> new ResponseEntity<>(nutzer, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/nutzers/name/{name}")
    public ResponseEntity<Nutzer> getNutzerByName(@PathVariable("name") String name){
        Optional<Nutzer> nutzerData = nutzerRepository.findByName(name);
        return nutzerData.map(nutzer -> new ResponseEntity<>(nutzer, HttpStatus.OK)).
                orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @PutMapping("/nutzers/{uid}")
    public ResponseEntity<Nutzer> updateNutzer(
            @PathVariable("uid") Integer uid, @RequestBody Nutzer nutzerDetails
    ){
        Optional<Nutzer> nutzerData = nutzerRepository.findById(uid);
        if (nutzerData.isPresent()){
            Nutzer n = nutzerData.get();
            n.setName(nutzerDetails.getName());
            n.setEmail(nutzerDetails.getEmail());
            return new ResponseEntity<>(nutzerRepository.save(n), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/nutzers/{uid}")
    public ResponseEntity<HttpStatus> deleteNutzer(
            @PathVariable("uid") Integer uid
    ){
        try {
            nutzerRepository.deleteById(uid);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/nutzers")
    public ResponseEntity<HttpStatus> deleteAllNutzers() {
        try {
            nutzerRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
