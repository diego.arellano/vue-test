package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

import java.util.Optional;

@SpringBootApplication
public class Demo1Application {
    private static final Logger log = LoggerFactory.getLogger(Demo1Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Demo1Application.class, args);
    }

    @Bean
    public CommandLineRunner demo(NutzerRepository repository){
        return (args) -> {
            // save Nutzer
//            repository.save(new Nutzer("jorge@test.de", "Papá"));

            // test findByName
            log.info("findByName:");
            Optional<Nutzer> n = repository.findByName("Matilda");
            log.info(n.toString());

            // fetch all Nutzer
            log.info("Nutzer found:");
            for (Nutzer nutzer: repository.findAll()){
                log.info(nutzer.toString());
            }
        };
    }

}
