# Simple Application: PostgreSQL + Spring + Vue

Based on https://www.bezkoder.com/vue-3-crud/

This subproject is my first attempt at connecting and working with a PostregSQL Database using Axios and Vue Router.

## Database
    CREATE TABLE Nutzer (
        uid     SERIAL PRIMARY KEY,
        email   VARCHAR(50),
        name    VARCHAR(100)
    );

    CREATE TABLE Freundschaft (
        uid1    INT NOT NULL,
        uid2    INT NOT NULL,
        Fgrad   VARCHAR(100),
        PRIMARY KEY( uid1, uid2 ),
        FOREIGN KEY (uid1) REFERENCES Nutzer (uid),
        FOREIGN KEY (uid2) REFERENCES Nutzer (uid)
    );

## Demo
![a demo of the Project](./demo.gif)